{
    "email_phone_number" : "reaganrealone@gmail.com",
    "password" : "admin.123"
    }
    
    INVALID RESPONSES
    This means the email address and password combination do not match
    {
    "success": false,
    "message": "INCORRECT_CREDENTIALS"
    }
    
    This means the supplied user email does not match any in the database
    {
    "success": false,
    "message": "STAFF_NOT_FOUND"
    }
    
    ==> Add a default error block
    
    {
    "success": false,
    "message": "STAFF_NOT_FOUND"
    }
    
    SUCCESSFUL RESPONSE
    {
    "success": true,
    "message": "LOGIN_SUCCESSFUL",
    }
export default {
    /* 
        VUE AXIOS API REQUEST FOR PATCH, PUT AND POSTS
    */
    async makeApiPatchOrPutOrPostRequest() {
​
        try {
            //Show Loading state
            this.isBusy = true;
​
            let response = await this.$http.post("main-route/sub-route", {
                prop1: "",
                prop2: "",
            });
            if (response.data.success && response.data.message == "SUCCESS MESSAGE"
            ) {
                //TODO: HANDLE SUCCESSFUL MESSAGE
            } else if (response.data.message == "ANY OTHER MESSAGE") {
                //TODO: HANDLE OTHER MESSAGE
            } else {
                //Report the Error to an error reporting service
                this.$rollbar.warning("STUDENTS FRONT END: Unexpected API response", {
                    response: response.data,
                    request: response,
                });
​
                //Throw this as an error
                throw "UNEXPECTED_RESPONSE_RECEIVED";
            }
        } catch (error) {
            //Handle Network Errors
            if (error.message === "Network Error") {
                return this.showFailedMessage(
                    "Connection Failed",
                    "Unable to Connect. Please check your Internet Connection and try again."
                );
            }
​
            //If the API returns other status codes besides 2xx, Handle these responses
            if (error.response) {
                if (error.response.data.message === "API MESSAGE HERE") {
                    //TODO: Handle this response
​
                    return;
                } else if (error.response.data.message === "OTHER API MESSAGE HERE") {
                    //TODO: Handle this response
​
                    return;
                }
            }
​
            //TODO: Handle default callback
​
        } finally {
            //Hide Loading state
            this.isBusy = false;
        }
    },
​
​
    /* 
       VUE AXIOS API REQUEST FOR GET
   */
    async makeApiGetRequest() {
​
        try {
            //Show Loading state
            this.isBusy = true;
​
            let response = await this.$http.get("main-route/sub-route");
            if (response.data.success && response.data.message == "SUCCESS MESSAGE"
            ) {
                //TODO: HANDLE SUCCESSFUL MESSAGE
            } else if (response.data.message == "ANY OTHER MESSAGE") {
                //TODO: HANDLE OTHER MESSAGE
            } else {
                //Report the Error to an error reporting service
                this.$rollbar.warning("STUDENTS FRONT END: Unexpected API response", {
                    response: response.data,
                    request: response,
                });
​
                //Throw this as an error
                throw "UNEXPECTED_RESPONSE_RECEIVED";
            }
        } catch (error) {
            //Handle Network Errors
            if (error.message === "Network Error") {
                return this.showFailedMessage(
                    "Connection Failed",
                    "Unable to Connect. Please check your Internet Connection and try again."
                );
            }
​
            //If the API returns other status codes besides 2xx, Handle these responses
            if (error.response) {
                if (error.response.data.message === "API MESSAGE HERE") {
                    //TODO: Handle this response
​
                    return;
                } else if (error.response.data.message === "OTHER API MESSAGE HERE") {
                    //TODO: Handle this response
​
                    return;
                }
            }
​
            //TODO: Handle default callback
​
        } finally {
            //Hide Loading state
            this.isBusy = false;
        }
    }
};
